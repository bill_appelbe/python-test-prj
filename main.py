import Shapes
import unittest
import math

class TestMethods(unittest.TestCase):

    def test_circle_perimeter(self):
        my_circle = Shapes.circle(3)
        self.assertEqual(2*math.pi*3, my_circle.perimeter())

    def test_circle_area(self):
        my_circle = Shapes.circle(3)
        self.assertEqual(math.pi*(3**2), my_circle.area())

    def test_rect_perimeter(self):
        my_rect = Shapes.rectangle(3,4)
        self.assertEqual(2*(3+4), my_rect.perimeter())

    def test_rect_area(self):
        my_rect = Shapes.rectangle(3,4)
        self.assertEqual(3*4, my_rect.area())

if __name__ == '__main__':
    unittest.main()

