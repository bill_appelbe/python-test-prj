
import math
class circle():
    def __init__(self,radius):
        self.radius=radius
    def area(self):
        return math.pi*(self.radius**2)
    def perimeter(self):
        return 2*math.pi*self.radius


class rectangle():
    def __init__(self,l, w):
        self.l = l
        self.w = w
    def area(self):
        return (self.l * self.w)
    def perimeter(self):
        return 3*(self.w + self.l)
